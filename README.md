# JSON-SCHEMA2

## what's this doing?

This is the sequal to [cl-json-schema](https://gitlab.com/Gnuxie/cl-json-schema).

Currently WIP, things probably are going to change without notice.

### The aims of this library are

* using json-schemas to define serializable clos classes.

### future features may include

* actual validation of instances against the schema using a metaclass that extends
the `json-clos.mop.serializable-class:serializable-class`

### problems this project probably is going to face.

* uris with json pointers and complicated referencing of schemas.

## LICENSE

This project is licensed under the Artistic License 2.0


