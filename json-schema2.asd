(asdf:defsystem #:json-schema2
  :description "protocol for validating json-schemas"
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :license  "Artistic License 2.0"
  :version "0.0.1"
  :depends-on ("json-schema2.util" "json-schema2.reader" "json-schema2.vocabulary"
                                   "json-schema2.annotator" "json-schema2.class-compiler")
  :serial t
  :components ((:module "code" :components
                        ((:file "package")
                         (:file "protocol")))) ;TODO
                                        ; references need to lookout for json pointers refering to the current document
  ; how do we accoutn for that in the reference resolvation protocol.
)
