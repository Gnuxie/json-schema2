#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:json-schema2.test)

(p:define-test annotation-inheritance
  :parent (#:json-schema2.test #:json-schema2.test))

(p:define-test type-inference
  :parent annotation-inheritance

  (let* ((config (make-instance 'jscc:configuration
                                :target-package
                                (jscc:ensure-package '#:json-schema2.test.annotation-inheritance)
                                :redefine-classes t))
         (defclass-forms
          (json-schema2:defclass<-options
              (asdf:system-relative-pathname '#:json-schema2.test
                                             "schema/annotation-inheritance.json")
              config)))
    (verbose:debug :annotation-inheritance "~s" defclass-forms)
    (eval defclass-forms))

  (let* ((annotation-test-alist
          '(("shelf" . ("CLCS" "LiSP"))))
         (instance
          (json-clos.serializer.alist:instance<-alist
           'json-schema2.test.annotation-inheritance::annotation-inheritance
           (copy-alist annotation-test-alist))))
    (p:true (equal '("CLCS" "LiSP")
                   (json-clos:pget instance '("shelf"))))))
