#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:json-schema2.test)

(p:define-test everything
  :parent json-schema2.test

  (let* ((config (make-instance 'jscc:configuration
                                :target-package (jscc:ensure-package '#:json-schema2.test.everything)
                                :redefine-classes t))
         (defclass-forms
          (json-schema2:defclass-forms<-directories config
              (architect.matrix-spec:spec  "event-schemas/schema/")))
         (making-slots-p nil))
    (v:debug :making-slots-p "~s" defclass-forms)
    (subst-if nil
              (lambda (tree)
                (when 
                    (and (listp tree)
                         (symbolp (car tree))
                         (string-equal "ROOM-VERSION" (car tree))
                         (member :initarg tree))
                  (setf making-slots-p t)
                  nil))
              defclass-forms)
    (p:true making-slots-p)))


;;; this tests the anotator protocol when the path is needed to be shifted
;;; to apply the allOf schemas to a schema-class ie changing the resource
;;; that the reader-schema refers to.
(p:define-test all-of-path-context
  :parent json-schema2.test

  (let* ((config (make-instance 'jscc:configuration
                                :target-package (jscc:ensure-package '#:json-schema2.test.everything)
                                :redefine-classes t))
         (defclass-forms
          (json-schema2:defclass<-options
              (architect.matrix-spec:spec "api/client-server/definitions/request_email_validation.yaml")
              config)))
    (v:debug :all-of-path-context "~s" defclass-forms)
    (eval defclass-forms))
  (let ((instance (make-instance 'json-schema2.test.everything::request-email-validation
                                 :id-server "id.example.com"
                                 :id-access-token "foo")))
    (p:is #'string= "id.example.com"
        (json-schema2.test.everything::id-server instance))))
