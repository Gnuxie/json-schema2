#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:json-schema2.test)

(p:define-test keyword-dispatch
  :parent (#:json-schema2.test #:json-schema2.test)

  (let* ((resource
         (js:find-resource
          (architect.matrix-spec:spec
           "event-schemas/schema/core-event-schema/room_event.yaml")))
         (schema (js:read-schema resource)))
    (step (format t "~a" schema))))
