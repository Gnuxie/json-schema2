#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:json-schema2.test)

(p:define-test json-pointer
    :parent (#:json-schema2.test #:json-schema2.test)

    (let ((resource
           (json-schema2.reader::resolve-uri
            (asdf:system-relative-pathname
             '#:json-schema2.test
             "schema/room_event.yaml/#/properties/room_id"))))
      (p:true (typep resource 'json-schema2.reader:resource-reference))
      (p:true (gethash "description" (json-schema2.reader:object resource)))))
