(asdf:defsystem #:json-schema2.test
  :description "regression tests for json-schema2"
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :license  "Artistic License 2.0"
  :version "0.0.1"
  :depends-on ("json-schema2" "parachute" "verbose" "architect.matrix-spec"
                              "the-cost-of-nothing"
                              "json-clos.serializer.jonathan")
  :serial t
  :components ((:file "package")
               (:module "class-compiler" :components
                        ((:file "defclass")))
               (:file "everything")
               (:file "json-pointer")
               (:file "items-test")
               (:module "pattern-property" :components
                        ((:file "package")
                         (:file "pattern")))
               (:file "annotation-inheritance")))
