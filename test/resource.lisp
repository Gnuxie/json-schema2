#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:json-schema2.test)

(p:define-test find-resource
  :parent json-schema2.test

  (let ((res
         (js:find-resource
          (asdf:system-relative-pathname '#:json-schema2.test
                                         "test/schema/event.yaml"))))
    (format t "~a" res)))
