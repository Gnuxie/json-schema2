#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:json-schema2.test
  (:use #:cl)
  (:local-nicknames (#:js #:json-schema2.reader)
                    (#:p #:parachute)
                    (#:jsr #:json-schema2.reader)
                    (#:jscc #:json-schema2.class-compiler)
                    (#:annot #:json-schema2.annotator))
  (:export
   #:json-schema2.test
   #:run
   #:ci-run))

(in-package #:json-schema2.test)

;;; test packages
(defpackage-plus-1:ensure-package '#:json-schema2.test.items-test)
(defpackage-plus-1:ensure-export '(#:items-test #:name #:description
                                   #:tags #:items-test-tag)
                                 '#:json-schema2.test.items-test)
(defpackage-plus-1:ensure-package '#:json-schema2.test.everything)
(defpackage-plus-1:ensure-package '#:json-schema2.test.annotation-inheritance)

(p:define-test json-schema2.test)

(defun run (&key (report 'p:plain))
  (p:test 'json-schema2.test :report report))

(defun ci-run ()
  (let ((verbose-level (verbose:repl-level))
        (test-result (progn (setf (verbose:repl-level) :debug) (run))))
    (setf (verbose:repl-level) verbose-level)
    (sleep 10) ; just let the output catch up, idk why it skips
    (when (not (null (p:results-with-status :failed test-result)))
      (uiop:quit -1))))
