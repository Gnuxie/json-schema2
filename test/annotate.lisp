#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:json-schema2.test)

(p:define-test annotator

  (let* ((resource
         (json-schema2:find-resource
          (architect.matrix-spec:spec
           "event-schemas/schema/m.room.create")))
         (schema
          (js:read-schema schema)))
    (let ((schema-class (json-schema2.class-compiler::apply-annotations
                         nil schema (json-schema2.class-compiler::make-schema schema))))
      (step (print schema-class)))))
