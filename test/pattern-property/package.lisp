#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:json-schema2.test.pattern-property-transformer
  (:use #:cl)
  (:local-nicknames
   (#:p #:parachute)
   (#:jc #:json-clos)
   (#:ppt #:json-schema2.pattern-property-transformer)
   (#:js #:json-schema2)
   (#:jsr #:json-schema2.reader)
   (#:jscc #:json-schema2.class-compiler))
  (:export
   #:pattern-property-transformer))

(in-package #:json-schema2.test.pattern-property-transformer)
(defpackage-plus-1:ensure-package '#:json-schema2.test.pattern.generated)
(defpackage-plus-1:ensure-export '(#:pattern-test))

(p:define-test json-schema2.test.pattern-property-transformer
  :parent (#:json-schema2.test #:json-schema2.test))
