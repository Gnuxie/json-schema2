#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:json-schema2.test.pattern-property-transformer)

;;; the name transform for pattern properties really sucks
(p:define-test pattern
  :parent json-schema2.test.pattern-property-transformer

  (let* ((config (make-instance 'jscc:configuration
                                :target-package (jscc:ensure-package '#:json-schema2.test.pattern.generated)
                                :redefine-classes t))
         (defclass-forms
          (js:defclass<-options
              (asdf:system-relative-pathname '#:json-schema2.test
                                             "schema/pattern-test.json")
              config)))
    (v:debug :pattern-property-transformer "~s" defclass-forms)
    (eval defclass-forms))
  (c2mop:finalize-inheritance (find-class 'json-schema2.test.pattern.generated::pattern-test))
  (c2mop:finalize-inheritance (find-class 'json-schema2.test.pattern.generated::|PATTERN-TEST-^([0-9]{3})$\|^(DEFAULT)$|))
  (let* ((alist-data
          '(("403" . (("message" . "FORBIDDEN")))))
         (instance (json-clos.serializer.alist:instance<-alist
                    'json-schema2.test.pattern.generated::pattern-test
                    (copy-alist alist-data))))
    (p:true (typep (json-clos:pget instance '("403"))
                   'json-schema2.test.pattern.generated::|PATTERN-TEST-^([0-9]{3})$\|^(DEFAULT)$|))
    (p:is #'string= "FORBIDDEN"
          (json-clos:pget instance '("403" "message")))))
