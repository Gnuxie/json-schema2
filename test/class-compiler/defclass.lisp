#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:json-schema2.test)

;;; currently the schema-class name is roughly~ the same as the eventual class-name
;;; and i don't think this is how it was intended to begin with.
;;; it's not. it's the immediate name, not a class-name ie for a slot.
;;; needs fixing immediatley.
;;; tests should be added here so it makes sense
(p:define-test defclass-generator
  :parent json-schema2.test

  (let* ((config (make-instance 'jscc:configuration
                                :target-package
                                (jscc:ensure-package
                                 '#:json-schema2.test.generated)
                                :redefine-classes t))
         (schema
          (js:read-schema
           (js:find-resource
            (architect.matrix-spec:spec
             "event-schemas/schema/m.room.create"))))
         (schema-class (json-schema2.annotator:apply-annotations
                        nil schema (json-schema2.annotator:make-schema schema)))
         (dependant-classes
          (jscc::depedant-classes<-schema schema-class config)))
    (verbose:debug :inner-classes "~s" dependant-classes)
    (loop :for slot :in (annot:slots schema-class)
       :do (p:true (find #\- (symbol-name (jscc::class-name<-schema slot config)) :test #'char=)
                   "schema-classes that are properties should have compound names."))))
