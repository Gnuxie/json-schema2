#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:json-schema2.test)

(p:define-test items
  :parent (#:json-schema2.test #:json-schema2.test))

(p:define-test items-test
  :parent items

  (let* ((config (make-instance 'jscc:configuration
                                :target-package
                                (jscc:ensure-package '#:json-schema2.test.items-test)
                                :redefine-classes t))
         (defclass-forms
          (json-schema2:defclass<-options
              (asdf:system-relative-pathname '#:json-schema2.test
                                             "schema/items-test.json")
              config)))    

    (verbose:debug :items-test "~s" defclass-forms)
    (eval defclass-forms))

  (let* ((item-instance (make-instance 'json-schema2.test.items-test:items-test-tag
                                       :name "cheese"
                                       :description "yellow"))
         (main-instance (make-instance 'json-schema2.test.items-test:items-test
                                       :tags (list item-instance)))

         (serial-form (jonathan:to-json main-instance))
         (from-serial (json-clos.serializer.jonathan:clos<-json
                       'json-schema2.test.items-test:items-test
                       serial-form)))
    (parachute:is #'string= "cheese"
                  (json-schema2.test.items-test:name
                   (first
                    (json-schema2.test.items-test:tags from-serial))))))
