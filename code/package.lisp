#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:json-schema2
  (:use #:cl)
  (:local-nicknames (#:jsr #:json-schema2.reader)
                    (#:vocab #:json-schema2.vocabulary)
                    (#:jcsc #:json-clos.mop.serializable-class)
                    (#:annot #:json-schema2.annotator)
                    (#:util #:json-schema2.util)
                    (#:jscc #:json-schema2.class-compiler))
  (:export
   #:ensure-class
   #:define-schema-class
   #:define-schema-spec
   #:defclass-forms<-directories
   #:defclass<-options))
