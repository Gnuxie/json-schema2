#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:json-schema2.annotator
  (:use #:cl #:json-schema2.util)
  (:local-nicknames (#:js #:json-schema2.reader)
                    (#:vocab #:json-schema2.vocabulary)
                    (#:util #:json-schema2.util))
  (:export
   #:schema-class
   #:name
   #:parent
   #:slots
   #:reader-schema
   #:annotations
   #:references
   #:resource
   #:apply-annotations
   #:make-schema
   #:direct-reference
   #:pattern-property
   #:pattern))
