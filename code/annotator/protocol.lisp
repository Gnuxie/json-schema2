#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:json-schema2.annotator)

;;; if you need slots for one of or something then you need to represent that with
;;; a new subclass of schema.
;;; this is a flat representation of the tree so if we have an all-of then proeprties
;;; and just a properties in the same schema, then they will be merged together.
;;; at the moment i'm just going to say that if there's a reference then we have
;;; to reference it by making it a superclass once we copmile
(defclass schema-class ()
  ((name :initarg :name :accessor name :type string
         :documentation "This is the immediate name of a schema-class
It should not be used as a class-name, it's important to make a distinction
because slot-names and initargs don't need a huge name.")

   (parent :initarg :parent :accessor parent :type (or schema-class null)
           :initform nil :documentation "The thing this came from.")

   (slots :initarg :slots :accessor slots :type list :initform (list))


   (annotations :initarg :annotations :accessor annotations
                :type list :initform  (list)
                :documentation "alist if annotations for this schema such as
description, required, type etc.")

   (references :initarg :references :accessor references
               :type list :initform (list)
               :documentation "Anything that this schema refreences with $ref
")

   (reader-schema :initarg :reader-schema :accessor reader-schema
                  :documentation "A resource if one was provided." )))

(defgeneric ensure-slot (schema property)
  (:method ((schema-class schema-class) (property vocab:property))
    (let* ((property-name (vocab:name property))
           (existing-property
            (find property-name (slots schema-class) :key #'name :test #'string=)))
      (or existing-property
          (let* ((next-resource (js:relative-resource
                                 (json-schema2.reader:resource
                                  (reader-schema schema-class))
                                 "properties"
                                 (vocab:name property)))
                 (p (make-instance 'schema-class
                                   :name property-name
                                   :parent schema-class
                                   :reader-schema (js:read-schema next-resource))))
            (push p (slots schema-class))
            p)))))

(defgeneric annotate (referencing-applicator referenced-applicator schema))

(defgeneric apply-annotations (referencing-applicator schema schema-class)
  (:method (p-app (schema js:schema) (schema-class schema-class))
    (dolist (applicator (js:applicators schema))
      (annotate p-app applicator schema-class))
    schema-class))

;;; going backwards is wrong when resolving parent schemas.
;;; becuase then we make schemas of things that aren't always schemas.
;;; like the object that is the value properties.
(defgeneric %make-schema (reader-schema resource)
  (:method ((reader-schema js:schema) (resource js:resource-reference))
    (let* ((parent-path-part ;; back by 2 because otherwise we will use the object part
            ;; of an applicator.
            (nreverse (cddr (reverse (js:reference resource)))))
           (parent-schema-class
            ;; TODO
            ;; this is going backwards but it'll do for now
            ;; we only really need to go backwards if the user isn't going to provide
            ;; a parent
            ;; so this whole make-schema thing is pretty sketchy.
            (if parent-path-part
                (make-schema
                 (js:read-schema
                  (apply #'js:relative-resource (js:base-resource resource)
                         parent-path-part)))
                (make-schema
                 (js:read-schema (js:base-resource resource))))))
      (make-instance 'schema-class
                     :name (princ-to-string (car (last (js:reference resource))))
                     :parent parent-schema-class
                     :reader-schema reader-schema)))
  (:method ((reader-schema js:schema) (resource js:resource))
    (make-instance 'schema-class
                   :name (symbol-name<-uri (js:base-uri resource))
                   :parent nil
                   :reader-schema reader-schema)))

(defgeneric make-schema (reader-schema)
  (:documentation "THIS IS ONLY USED FOR NEW ROOT SCHEMA-CLASSES")
  (:method ((reader-schema js:schema))
    (%make-schema reader-schema (js:resource reader-schema))))

(defmethod print-object ((object schema-class) s)
  (print-unreadable-object (object s :type t :identity t)
    (format s "~s" (name object))))
