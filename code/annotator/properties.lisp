#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:json-schema2.annotator)

(defmethod annotate (p-app (r-app vocab:properties) (schema-class schema-class))
  (flet ((traverse-property (property)
           (let ((p (ensure-slot schema-class property)))
             (apply-annotations r-app (vocab:schema property) p))))
    (loop :for property :in (vocab:properties r-app)
       :do (traverse-property property))))

(defclass pattern-property ()
  ((pattern :initarg :pattern
            :reader pattern
            :type string)
   (schema-class :initarg :schema-class
                 :reader schema-class
                 :type schema-class)))

(defmethod annotate (p-app (r-app vocab:pattern-properties) (schema-class schema-class))
  (let ((patterns
         (loop :for property :in (vocab:properties r-app)
            :collect (let* ((pattern-property-schema
                            (js:read-schema
                             (js:relative-resource
                              (js:resource (reader-schema schema-class))
                              "patternProperties"
                              (vocab:name property))))
                            (pattern-property-schema-class
                             (make-instance 'schema-class
                                            :name (vocab:name property)
                                            :parent schema-class
                                            :reader-schema pattern-property-schema)))
                       (apply-annotations nil pattern-property-schema
                                          pattern-property-schema-class)
                       (make-instance 'pattern-property
                                      :pattern (vocab:name property)
                                      :schema-class pattern-property-schema-class)))))
    (setf (references schema-class)
          (append (references schema-class)
                  patterns))))

(defun keyword<-json-schema-type (schema-type)
  (cond
    ((listp schema-type)
     (mapcar #'keyword<-json-schema-type schema-type))
    (t
     (alexandria:switch (schema-type :test #'string=)
       ("string" :string)
       ("integer" :integer)
       ("number" :number)
       ("object" :object)
       ("array" :array)
       ("boolean" :boolean)
       ("null" :null)))))

(defmethod annotate (p-app (json-type vocab:json-type) (schema-class schema-class))
  (setf (annotations schema-class)
        (acons :json-type
               (keyword<-json-schema-type (vocab:json-type json-type))
               (annotations schema-class))))

(defmethod annotate (p-app (description vocab:description) (schema-class schema-class))
  (setf (annotations schema-class)
        (acons :description (vocab:description description) (annotations schema-class))))

(defmethod annotate (p-app (r-app vocab:title) (schema-class schema-class)))



;;; TODO this is pretty telling of a flaw.
(defmacro with-shifted-reader-schema ((schema-class &rest path) &body body)
  (alexandria:with-gensyms (old-reader-schema)
    `(let ((,old-reader-schema (reader-schema ,schema-class)))
       (setf (reader-schema ,schema-class)
             (js:read-schema (apply #'js:relative-resource (js:resource ,old-reader-schema)
                                    (list ,@path))))
       ,@body
       (setf (reader-schema ,schema-class)
             ,old-reader-schema))))

(defmethod annotate (p-app (r-app vocab:all-of) (schema-class schema-class))
  (loop :for schema :in (vocab:schemas r-app)
     :for i :from 0
     :do (with-shifted-reader-schema (schema-class "allOf" i)
            (apply-annotations r-app schema schema-class)))
  schema-class)

(defclass direct-reference ()
  ((uri :initarg :uri :reader uri
        :type string)

   (schema-class :initarg :schema-class :accessor schema-class
                 :type schema-class)))

(defmethod print-object ((object direct-reference) s)
  (print-unreadable-object (object s :type t :identity t)
    (format s "~s" (uri object))))

(defmethod annotate (p-app (r-app vocab:ref) (schema-class schema-class))
  (let* ((reference-resource
          (js:relative-resource* (json-schema2.reader:resource
                                  (reader-schema schema-class))
                                 (vocab:uri r-app)))
         (reference-schema (js:read-schema reference-resource))
         (reference-schema-class (make-schema reference-schema)))
    (apply-annotations nil reference-schema reference-schema-class)
    (push (make-instance 'direct-reference
                         :uri (vocab:uri r-app)
                         :schema-class reference-schema-class)
          (references schema-class))))

(defmethod annotate (p-app (r-app vocab:items) (schema-class schema-class))
  (setf (annotations schema-class)
        (acons :items
               (loop :for reader-schema :in (vocab:item-schemas r-app)
                    :for i :from 0
                  :collect
                    (let ((item-schema-class
                           (make-instance 'schema-class
                                          :parent schema-class
                                          :name (princ-to-string i)
                                          :reader-schema reader-schema)))
                      (apply-annotations r-app reader-schema item-schema-class)))
               (annotations schema-class))))

(defmethod annotate (p-app (r-app vocab:required) (schema-class schema-class)))

