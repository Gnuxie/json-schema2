(asdf:defsystem #:json-schema2.annotator
  :description "A annotator for the class compiler"
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :licence "Artistic License 2.0"
  :depends-on ("json-schema2.reader" "json-schema2.vocabulary")
  :serial t
  :components ((:file "package")
               (:file "protocol")
               (:file "properties")))
