#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:json-schema2)

(defgeneric defclass<-options (uri configuration)
  (:method (uri (config jscc:configuration))
    (handler-bind
        ;; skipping was added to debug a problem with how uiop:directory-files
        ;; was used.
        ((jsr:not-a-schema (lambda (c)
                             (when (and (jscc:skip-bad-schemas config)
                                        ;; it's important that we skip this one
                                        ;; if it's been given as a directory mapping
                                        ;; rather than as a reference in a schema.
                                        (and (pathnamep uri) (pathnamep (jsr:uri c)))
                                        (equal (jsr:uri c) uri))
                               (warn "skipping ~s as it's not a schema"
                                     (jsr:uri c))
                               (return-from defclass<-options nil))))

         (yaml.error:parsing-error (lambda (c)
                                     ;; unfortunatley we can't give any guarntees
                                     ;; whether this came from a referrenced schema or not.
                                     (when (jscc:skip-bad-schemas config)
                                       (warn "skipping ~s with parse error ~a" uri c)
                                       (return-from defclass<-options nil)))))
      (let* ((resource
              (jsr:find-resource uri))
             (schema (jsr:read-schema resource))
             (schema-class (annot:make-schema schema))
             
             (defclass-forms
              (progn (annot:apply-annotations nil schema schema-class)
                     (jscc:defclass<-schema schema-class config))))
        defclass-forms))))

(defgeneric ensure-class (uri target-package)
  (:documentation "Uses eval, probably not a good idea.")
  (:method (uri target-package)
    (eval (defclass<-options uri (make-instance 'jscc:configuration
                                                :target-package (jscc:ensure-package
                                                                 target-package))))))

(defgeneric defclass-forms<-directories (configuration &rest uris)
  (:method ((config jscc:configuration) &rest uris)
    `(progn
       ,@(loop :for spec :in uris
            :collect `(progn
                        ,@ (mapcar (lambda (f) (defclass<-options f config))
                                   (uiop:directory-files spec)))))))

(defmacro define-schema-class ((package-designator &key redefine-classes) uri)
  (let ((config (make-instance 'jscc:configuration
                               :target-package (jscc:ensure-package package-designator)
                               :redefine-classes redefine-classes)))
    `(progn (defpackage+-1:ensure-package ',package-designator)
       ,(defclass<-options uri config))))

(defmacro define-schema-spec ((package-designator &key redefine-classes skip-bad-schemas)
                              &rest specification-directories)
  (let ((config (make-instance 'jscc:configuration
                               :target-package (jscc:ensure-package package-designator)
                               :redefine-classes redefine-classes
                               :skip-bad-schemas skip-bad-schemas)))
    `(progn (defpackage+-1:ensure-package ',package-designator)
            ,(apply #'defclass-forms<-directories config specification-directories))))
