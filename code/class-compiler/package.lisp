#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:json-schema2.class-compiler
  (:use #:cl)
  (:local-nicknames (#:js #:json-schema2.reader)
                    (#:vocab #:json-schema2.vocabulary)
                    (#:jcsc #:json-clos.mop.serializable-class)
                    (#:annot #:json-schema2.annotator)
                    (#:util #:json-schema2.util)
                    (#:ppt #:json-schema2.pattern-property-transformer)
                    (#:jc #:json-clos))
  (:export
   ;; annotator
   #:apply-annotations
   #:make-schema
   #:schema

   ;; generator
   #:defclass<-schema
   #:ensure-package

   ;; config
   #:configuration
   #:skip-bad-schemas
   #:name-table
   ))
