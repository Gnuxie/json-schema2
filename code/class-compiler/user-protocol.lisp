#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:json-schema2.class-compiler)

(defun ensure-package (package-designator)
  (defpackage+-1:ensure-package package-designator)
  (find-package package-designator))
