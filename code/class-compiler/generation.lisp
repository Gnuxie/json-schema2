#|
    Copyright (C) 2019-2020 Gnuxie <Gnuxie@protonmail.com>
    
    part of this file comes from the original generation-protocol
    in cl-json-schema
|#

(in-package #:json-schema2.class-compiler)

(defgeneric compile-schema (schema config)
  (:documentation "This is just the top level entrance using one of our schemas."))

;;; there should be a second generic for slot initargs
;;; this way you can add more of them without messing up the method i guess.
(defgeneric slot-initarg<-annotation (schema config annotation association)
  (:documentation "create an initarg from the annotation
doesn't need to be overriden but is useful.")
  (:method ((schema annot:schema-class) (config configuration) annot assoc)
    nil))

(defgeneric find-direct-annotation (annotation schema config)
  (:documentation "There's no idea about precedence here
What if two references in allOf have an :items annoation?

It depends whether itmes has more than one schema, if there's just one
then you'd have to inherit all the other items for it to be correct.

And that still isn't how json-schema works, you would need to merge annotations
ie the schema from the reference would have to be merged into the referncing schema
on an applicator basis and signaling whenever there's a conflict. This is the only
real way to be correct.")
  (:method (annotation (schema annot:schema-class) (config configuration))
    (let ((current-annotation (cdr (assoc annotation (annot:annotations schema)))))
      (or current-annotation
          (loop :for direct-reference :in (direct-references<-schema schema config)
             :do (setf current-annotation
                       (find-direct-annotation annotation
                                               (annot:schema-class direct-reference)
                                               config))
             :if current-annotation :do (return-from nil current-annotation))))))

(defun cl-type<-json-schema-type (type)
  (etypecase type
    (null 'null)
    (list
     `(or ,@ (mapcar #'cl-type<-json-schema-type type)))
    (keyword
     (case type
       (:string 'string)
       (:integer 'integer)
       (:number 'number)
       (:object '(or jcsc:json-serializable list))
       (:array 'list)
       (:boolean 'boolean)
       (:null 'null)))))

(defun json-clos-type<-json-schema-type (type schema config)
  (etypecase type
    (null nil)
    (list
     `(or ,@ (mapcar #'cl-type<-json-schema-type type)))
    (keyword
     (case type
       (:object (or (%mop-class-name<-sub-spec schema config) :object))
       (t type)))))

(defmethod slot-initarg<-annotation ((schema annot:schema-class) (config configuration)
                                     (annot (eql :json-type)) association)
  `(:type ,(cl-type<-json-schema-type association)))

(defgeneric %mop-class-name<-sub-spec (schema-class config)
  (:method ((schema-class annot:schema-class) (config configuration))
    (let ((effective-schema-class
           (cond ((and (= 1 (length (annot:references schema-class)))
                       (typep (car (annot:references schema-class)) 'annot:direct-reference)
                       (null (annot:slots schema-class))
                       (null (annot:annotations schema-class)))
                  (annot:schema-class (car (annot:references schema-class))))

                 (t schema-class))))
      (class-name<-schema effective-schema-class config))))

;;; TODO
;;; at the moment we do not create any schema for schemas that are of a type
;;; that isn't an object, not sure how big of a problem this is.
(defgeneric json-clos-type<-json-schema (schema config)
  (:method ((schema annot:schema-class) (config configuration))
    (let ((items (find-direct-annotation :items schema config))
          (schema-type (find-direct-annotation :json-type schema config)))
      (cond ((= 1 (length items))
             `(:array ,(json-clos-type<-json-schema-type
                        (find-direct-annotation :json-type (first items) config)
                        (first items) config)))
            

            ((null items)
             (json-clos-type<-json-schema-type schema-type schema config))
            
            (t `(:array (or
                         ,@ (loop :for item-spec :in items
                               :collect (json-clos-type<-json-schema-type
                                         (find-direct-annotation :json-type item-spec config)
                                         item-spec config)))))))))

(defgeneric slot-option<-schema (schema config)
  (:documentation "Create a slot-option from the schema for it's parent.")
  (:method ((schema annot:schema-class) (config configuration))
    (let ((initargs
           (loop :for annot :in (annot:annotations schema)
              :append (slot-initarg<-annotation schema config
                                                (car annot) (cdr annot))))
          (symbol-name (util:symbol<-key (annot:name schema) :package (target-package config))))
      `(,symbol-name
        :initarg ,(util:keyword<-key (annot:name schema))
        :accessor ,symbol-name
        :json-key ,(annot:name schema)
        :json-type ,(json-clos-type<-json-schema schema config)
        ,@initargs))))

(defgeneric slot-options<-schema (schema config)
  (:method ((schema annot:schema-class) (config configuration))
    (loop :for slot-schema :in (annot:slots schema)
       :collect (slot-option<-schema slot-schema config))))

(defgeneric class-options<-schema (schema config)
  (:method ((schema annot:schema-class) (config configuration))
    (declare (ignore schema config))
    '((:metaclass jcsc:json-serializable-class))))

;;; this needs to do references too and we need to lookup -
;;; then we get to this properties: - foo problem.
;;; that we're just going to ignore for now tbh.
;;; one way of solving this is to resolve references during annotation
;;; and annotate the cc:schema with things to inherit.

;;; one solution here is to lookup to the references with the path we have
;;; and look for this slot in all the other shema-classes
;;; then inherit if they exist.
(defgeneric direct-references<-schema (schema config)
  (:method ((schema annot:schema-class) (config configuration))
    (remove-if-not (lambda (d)
                            (typep d 'annot:direct-reference))
                          (annot:references schema))))

(defgeneric superclass-list<-schema (schema config)
  (:method ((schema annot:schema-class) (config configuration))
    (mapcar (lambda (d)
              (class-name<-schema (annot:schema-class d) config))
            (direct-references<-schema schema config))))

;;; TODO
;;; maybe we create some protocol for handling dependant classes
;;; ie a gf that when given a direct-reference or pattern-property
;;; it can just give us a defclass form.
(defgeneric depedant-classes<-schema (schema config)
  (:method ((schema annot:schema-class) (config configuration))
    (append
     (loop :for direct-reference :in (direct-references<-schema schema config)
        :collect (defclass<-schema (annot:schema-class direct-reference) config))
     (slot-classes<-schema schema config)
     (items-classes<-schema schema config)
     (pattern-classes<-schema schema config))))

(defgeneric slot-classes<-schema (schema config)
  (:method ((schema annot:schema-class) (config configuration))
    (loop :for slot-schema :in (annot:slots schema)
       :when (cdr (assoc :items (annot:annotations slot-schema)))
       :append (items-classes<-schema slot-schema config)
       :collect (defclass<-schema slot-schema config))))

(defgeneric items-classes<-schema (schema config)
  (:method ((schema annot:schema-class) (config configuration))
    (let ((items (cdr (assoc :items (annot:annotations schema)))))
      (when items
        (loop :for item-schema :in items
           :collect (defclass<-schema item-schema config))))))

(defgeneric pattern-properties<-schema (schema config)
  (:method ((schema annot:schema-class) (cnofig configuration))
    (remove-if-not (lambda (sc)
                     (typep sc 'annot:pattern-property))
                   (annot:references schema))))

(defgeneric pattern-classes<-schema (schema config)
  (:method ((schema annot:schema-class) (config configuration))
    (let ((patterns (pattern-properties<-schema schema config)))
      (when patterns
        (loop :for pattern :in patterns
           :collect (defclass<-schema (annot:schema-class pattern)
                        config))))))

(defgeneric pattern-property-transformers<-schema (schama config)
  (:documentation "Create the forms adding the json-clos:pattern-properties
to a json-clos:json-serializable-class.

TODO Pattern properties also has this problem where we don't know what to do if the
schema type is not object ")
  (:method ((schema annot:schema-class) (config configuration))
    (let* ((patterns (pattern-properties<-schema schema config))
           (pattern-intern-forms
            (loop :for pattern :in patterns
               :for json-clos-type := (json-clos-type<-json-schema
                                        (annot:schema-class pattern)
                                        config)
               :when (and (not (or (keywordp json-clos-type)
                                   (null json-clos-type)))
                          (symbolp json-clos-type))
               :collect `(jc:add-transformer
                          (make-instance 'ppt:pattern-property-transformer
                                         :pattern ',(annot:pattern pattern)
                                         :schema-class ',json-clos-type)
                          (find-class ',(class-name<-schema schema config))))))
      (when patterns
        `(eval-when (:compile-toplevel :load-toplevel :execute)
           ,@pattern-intern-forms)))))

(defgeneric debug-info<-schema (schema config)
  (:method ((schema annot:schema-class) (config configuration))
    (when (debug-mode config)
      (let* ((resource (js:resource (annot:reader-schema schema)))
             (resource-info
              (typecase resource
                (js:resource "BASE RESOURCE")
                (js:resource-reference
                 (format nil "PATH ~s" (js:reference resource)))))
             (schema-info
              (format nil "~%SCHEMA-CLASS: ~a
HAS-PARENT ~A,~%~a slot~:p,
~s annotation~:p,~%~a reference~:p"
                      schema
                      (annot:parent schema)
                      (annot:slots schema)
                      (annot:annotations schema)
                      (annot:references schema))))
        (concatenate 'string resource-info schema-info)))))

(defgeneric defclass<-schema (schema config)
  (:documentation "Create a defclass form from the schema")
  (:method ((schema annot:schema-class) (config configuration))
    (let* ((name (class-name<-schema schema config))
           (defclass-form
            `(progn
               ,(debug-info<-schema schema config)
               (defclass ,name
                   ,(superclass-list<-schema schema config) 
                 ,(slot-options<-schema schema config)
                 ,@ (class-options<-schema schema config))
               ,(pattern-property-transformers<-schema schema config) 
               (c2mop:finalize-inheritance (find-class ',name)))))
      (unless (class-defined-p name config)
        (setf (class-defined-p name config) t)
        `(eval-when (:compile-toplevel :load-toplevel :execute)
           ,@ (depedant-classes<-schema schema config)
           ,(if (redefine-classes config)
                defclass-form
                `(unless (find-class ',name nil)
                   ,defclass-form)))))))
