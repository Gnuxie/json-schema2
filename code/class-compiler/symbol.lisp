#|
    Copyright (C) 2019-2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:json-schema2.class-compiler)


(defgeneric name-value (schema/resource uri)
  (:method ((resource js:resource) uri)
    (list uri))
  (:method ((resource js:resource-reference) uri)
    (append (name-value (js:base-resource resource) uri)
            (js:reference resource)))
  (:method ((schema annot:schema-class) uri)
    (name-value (js:resource (annot:reader-schema schema)) uri)))

(defun %ensure-unique-name (schema name name-table uri attempts)
  (declare (type annot:schema-class schema))
  (let ((name-value (name-value schema uri)))
    (let ((entry (gethash name name-table)))
      (cond ((and entry (equal entry name-value)) name)

            ((and entry (not (equal entry name-value)))
             (%make-class-name schema uri name-table (1+ attempts)))

            (t (setf (gethash name name-table)
                     name-value)
               name)))))

(defun %construct-class-name (schema name-parts)
  "return a list of name components that can be added to later on."
  (declare (type list name-parts))
  (declare (type annot:schema-class schema))
  (let ((next-name-parts
         (if name-parts
             (append (list (annot:name schema) "-") name-parts)
             (list (annot:name schema)))))
    (if (not (annot:parent schema))
        next-name-parts
        (%construct-class-name (annot:parent schema) next-name-parts))))

(defgeneric %make-class-name (schema uri name-table &optional attempts)
  (:documentation "This sucks but it's a good way around the problem of files
named the same for now because you can't always backtrack up the directory tree
to generate a name, since sometimes it will just not make any sense.")
  (:method ((schema annot:schema-class) (uri pathname) name-table &optional (attempts 0))
    (let* ((name-components
            (append
             (%construct-class-name schema nil)
             (unless (= 0 attempts)
               (list "-" (princ-to-string attempts)))))

           (raw-name
            (apply #'concatenate 'string name-components)))
      (%ensure-unique-name schema raw-name name-table uri attempts))))

(defgeneric class-name<-schema (schema config)
  (:method ((schema annot:schema-class) (config configuration))
    (let ((name (%make-class-name schema
                                  (js:base-uri (js:resource
                                                (annot:reader-schema schema)))
                                  (name-table config))))
      (util:symbol<-key name :package (target-package config)))))

(defgeneric short-name<-schema (schema config)
  (:documentation "This name is to be used for initargs, accessors and slot-names")
  (:method ((schema annot:schema-class) (config configuration))
    (util:symbol<-key (annot:name schema) :package (target-package config))))
