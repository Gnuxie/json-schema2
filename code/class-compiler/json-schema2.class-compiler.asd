(asdf:defsystem #:json-schema2.class-compiler
  :description "A class compiler for json-schema2"
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :licence "Artistic License 2.0"
  :depends-on ("json-schema2.reader" "json-schema2.vocabulary" "json-schema2.annotator"
                                     "json-clos" "cl-change-case" "defpackage-plus"
                                     "json-schema2.pattern-property-transformer")
  :serial t
  :components ((:file "package")
               (:file "configuration")
               (:file "symbol")
               (:file "generation")
               (:file "user-protocol")))
