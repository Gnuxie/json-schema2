#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:json-schema2.class-compiler)

(defvar *debug-info* nil)

;;; the top level function will instantiate this class from the arguments
;;; e.g. someone will provide a package designator and if that package
;;; doesn't exist it will make it and instantiate this class and wack it
;;; into the generation protocol.
(defclass configuration ()
  ((target-package :initarg :target-package :accessor target-package
                   :type package :initform nil)

   (redefine-classes :initarg :redefine-classes :accessor redefine-classes
                     :type boolean :initform nil
                     :documentation "Whether to redefine classes that already exist.")

   (skip-bad-schemas :initarg :skip-bad-schemas :reader skip-bad-schemas
                     :type boolean :initform nil
                     :documentation "Whether to skip schemas that aren't schemas.")

   (name-table :initarg :name-table :reader name-table
               :type hash-table :initform (make-hash-table :test #'equal)
               :documentation "NAME => URI")

   (defined-classes :initarg :defined-classes :reader defined-classes
                    :type hash-table :initform (make-hash-table))

   (debug-mode :initarg :debug-mode :reader debug-mode
               :type boolean :initform *debug-info*)))

(defgeneric class-defined-p (class-name config)
  (:method ((class-name symbol) (config configuration))
    (gethash class-name (defined-classes config))))

(defmethod (setf class-defined-p) (value (class-name symbol) (config configuration))
  (setf (gethash class-name (defined-classes config))
        value))
