(asdf:defsystem #:json-schema2.util
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :license  "Artistic License 2.0"
  :depends-on ("defpackage-plus")
  :serial t
  :components ((:file "util")))
