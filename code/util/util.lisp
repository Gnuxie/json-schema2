#|
    Copyright (C) 2019-2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:json-schema2.util
  (:use #:cl)
  (:export
   #:%symbol<-key
   #:symbol<-key
   #:keyword<-key
   #:symbol-name<-uri
   #:sub-uri))

(in-package #:json-schema2.util)

(declaim (inline %symbol<-key symbol<-key keyword<-key))
(defun %symbol<-key (key)
  (declare (type string key))
  (string-upcase (substitute #\- #\_ key)))

(defun symbol<-key (key &key (package *package*) (export t))
  (let ((sym (intern (%symbol<-key key) package)))
    (when export
      (defpackage+-1:ensure-export (list sym) package))
    sym))

(defun keyword<-key (key)
  (intern (%symbol<-key key) :keyword))

(defgeneric symbol-name<-uri (path)
  (:method ((path pathname))
    (alexandria:switch ((pathname-type path) :test #'string=)
      ("yaml" (pathname-name path))
      ("json" (pathname-name path))
      (t (file-namestring path)))))

(defgeneric sub-uri (base-uri uri)
  (:method ((base-uri pathname) (uri pathname))
    (uiop:subpathname base-uri uri))
  (:method ((u1 string) (u2 string))
    (sub-uri (pathname u1) (pathname u2)))
  (:method ((u1 pathname) (u2 string))
    (sub-uri u1 (pathname u2))))
