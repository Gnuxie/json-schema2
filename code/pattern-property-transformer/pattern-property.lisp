#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:json-schema2.pattern-property-transformer)

(defclass pattern-property-transformer (jc:additional-property-transformer)
  ((pattern :initarg :pattern
            :reader pattern
            :type string)
   (schema-class :initarg :schema-class
                 :reader schema-class
                 :documentation "The name of a json-serializable-class
to instantiate an additional property matching the pattern with.")))

(defmethod jc:transform (instance class (transformer pattern-property-transformer)
                           property-name property)
  (when (cl-ppcre:scan (pattern transformer) property-name)
    (values
     (json-clos.serializer.alist:instance<-alist
      (schema-class transformer) property)
     t)))

(defmethod jc:add-transformer ((transformer pattern-property-transformer)
                               (class jc:json-serializable-class))
  (pushnew transformer (jc:additional-property-transformers class)
           :test (lambda (existing-transformer transformer)
                   (and (typep existing-transformer 'pattern-property-transformer)
                        (string= (pattern existing-transformer)
                                 (pattern transformer))
                        (eql (schema-class transformer)
                             (schema-class existing-transformer))))))
