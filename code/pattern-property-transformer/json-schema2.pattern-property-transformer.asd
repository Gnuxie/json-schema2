(asdf:defsystem #:json-schema2.pattern-property-transformer
  :description "A json-clos additonal-property-transformer for json-schema pattern properties"
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :licence "Artistic License 2.0"
  :depends-on ("json-clos" "cl-ppcre" "json-clos.serializer.alist")
  :serial t
  :components ((:file "package")
               (:file "pattern-property")))
