#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:json-schema2.pattern-property-transformer
  (:use #:cl)
  (:local-nicknames
   (#:jc #:json-clos))
  (:export
   #:pattern-property-transformer
   #:pattern
   #:schema-class))
