#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:json-schema2.reader)

#+ (or) ; can't remembre why this was needed.
(defvar *applicators* (make-hash-table :test 'equal))

(defclass applicator-class (c2mop:standard-class)
  ((keyword-name :reader keyword-name
         :type string
         :documentation "The name of the applicator, used by depends-on
to search for this applicator in a schema's applicator list.")

   (depends-on :reader depends-on
               :type list
               :documentation "string property names of other applicators which
this applicator depends-upon

used to find them in a schema with (lambda (a) (string= dependant (name (class-of a))))")))

(defmethod c2mop:validate-superclass ((class applicator-class)
                                           (super closer-mop:standard-class))
  t)

(defmethod c2mop:validate-superclass ((class standard-class)
                                           (super applicator-class))
  t)

(defmethod shared-initialize :before ((class applicator-class) slot-names
                                      &key keyword-name depends-on)
  (declare (ignore slot-names))
  (if keyword-name
      (setf (slot-value class 'keyword-name) (car keyword-name))
      (error "You must supply a name to an applicator-class"))
  (setf (slot-value class 'depends-on)
        depends-on)
  class)

(defclass applicator ()
  ())
