#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:json-schema2.reader)

(define-condition not-a-schema (error)
  ((%uri :initarg :uri
         :initform nil
         :reader uri)

   (%resource :initarg :resource
              :initform nil
              :reader resource))
  (:documentation "error for when we try to locate a resource that isn't a schema."))

(defgeneric uri+path<-json-pointer-uri (uri)
  (:method ((string-uri string))
    (let* ((decoded-uri (do-urlencode:urldecode string-uri))
           (pointer-start (position #\# string-uri :test #'char=))
           (path (split-sequence:split-sequence #\/ (subseq decoded-uri
                                                            (+ 2 pointer-start))))
           (new-uri (subseq string-uri 0 pointer-start)))
      (values new-uri path)))
  (:method ((pathname pathname))
    (multiple-value-bind (next-uri path)
        (uri+path<-json-pointer-uri (namestring pathname))
      (values (pathname next-uri) path))))

(defgeneric resource<-json-pointer-uri (uri)
  (:method ((uri pathname))
    (multiple-value-bind (next-uri path)
        (uri+path<-json-pointer-uri uri)
      (let ((resource (resolve-uri next-uri)))
        (apply #'relative-resource resource path)))))

(defgeneric resolve-uri (uri)
  (:method (uri)
    (verbose:debug :json-schema2.reader "looking for ~a" uri)
    (if (find #\# (namestring uri) :test #'char=)
        (resource<-json-pointer-uri uri)
        (let ((resource (yaml:parse uri)))
          (typecase resource
            (hash-table (make-instance 'resource :object resource
                                       :base-uri uri))
            (t (error (make-condition 'not-a-schema :uri uri :resource resource))))))))

(defclass resource ()
  ((object :initarg :object
           :accessor object
           :type hash-table
           :documentation "The parsed json schema object")

   (%base-uri :initarg :base-uri
              :reader base-uri
              :documentation "RFC 3986 SECTION 5.")))

(defgeneric find-resource (uri)
  (:documentation "create a resource from the uri.")

  (:method ((uri pathname))
    (let ((resource (resolve-uri uri)))
      resource)))

(defmethod print-object ((resource resource) stream)
  (print-unreadable-object (resource stream :type t :identity t)
    (format stream "BASE-URI: ~s" (base-uri resource))))

(defclass resource-reference ()
  ((base-resource :initarg :base-resource
                  :accessor base-resource
                  :type resource
                  :documentation "the base resource")

   (reference :initarg :reference
              :accessor reference
              :documentation "A cl-hash-util path.")))

(defmethod object ((resource-reference resource-reference))
  (with-accessors ((base-resource base-resource) (reference reference))
      resource-reference
    (cl-hash-util:hget (object base-resource) reference)))

(defgeneric relative-resource (resource &rest path)
  (:method ((resource resource) &rest path)
    (make-instance 'resource-reference
                   :base-resource resource
                   :reference path))

  (:method ((resource resource-reference) &rest path)
    (make-instance 'resource-reference
                   :base-resource (base-resource resource)
                   :reference (append (reference resource) path))))

(defmethod base-resource ((resource resource))
  resource)

(defgeneric relative-resource* (resource uri)
  (:method (resource (uri pathname))
    (if (find #\# (namestring uri))
        (multiple-value-bind (next-uri path)
            (uri+path<-json-pointer-uri uri)
          (if (string= "" (namestring next-uri))
              (apply #'relative-resource (base-resource resource) path)
              (resolve-uri uri)))
        (let ((uri (util:sub-uri (base-uri resource) uri)))
          (resolve-uri uri))))
  (:method (resource (uri string))
    ;; honestly we need some kind of uri detection mechanism in future
    (relative-resource* resource (pathname uri))))

(defmethod base-uri ((resource resource-reference))
  "This is fine because we are only using this to lookup other references with."
  (base-uri (base-resource resource)))
