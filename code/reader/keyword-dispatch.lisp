#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:json-schema2.reader)

(defvar *keywords* (make-hash-table :test 'equal)
  "These are keywords that map to a function accepting (keyword resource schema)")

(defgeneric keyword-dispatch (keyword resource schema)
  (:documentation "prepare the tree stuffs for this keyword on the given schema.")
  (:method (keyword resource schema)
    (anaphora:aif (gethash keyword *keywords*)
                  (funcall anaphora:it keyword resource schema)
                  (format t "~&WARNING: unimplemented keyword ~a" keyword))))

(defgeneric read-schema (resource)
  (:method (resource)
    (let ((schema (make-instance 'schema :resource resource)))
      (maphash (lambda (key value)
                 (declare (ignore value))
                 (keyword-dispatch key (relative-resource resource key) schema))
               (object resource))
      schema))
  #+ (or)
  (:method ((schema resource-schema))
    (maphash (lambda (key value)
                 (declare (ignore value))
                 (keyword-dispatch key (relative-resource schema key) schema))
             (object schema))
    schema))

(defun intern-keyword (keyword acceptor)
  (setf (gethash keyword *keywords*) acceptor))

(defmacro define-keyword (keyword (&rest lambda-list) &body body)
  `(progn
     (intern-keyword ,keyword
                     (lambda ,lambda-list
                       ,@body))))
