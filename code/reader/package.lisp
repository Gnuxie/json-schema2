#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:json-schema2.reader
  (:use #:cl)
  (:local-nicknames (#:util #:json-schema2.util))
  (:export
   ;; resource.lisp
   #:resource
   #:object
   #:base-uri
   #:find-resource
   #:resource-reference
   #:base-resource
   #:reference
   #:relative-resource
   #:relative-resource*
   #:not-a-schema
   #:uri

   ;; schema.lisp
   #:schema
   #:applicators
   #:add-applicator
   #:base-uri
   #:canonical-uri
   #:keyword-dispatch

   ;; applicator.lisp
   #:applicator-class
   #:applicator
   #:keyword-name
   #:depends-on
   #:find-applicator

   ;; keyword-dispatch.lisp
   #:*keywords*
   #:keyword-dispatch
   #:read-schema
   #:define-keyword))
