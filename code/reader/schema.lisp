#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:json-schema2.reader)

(defclass schema ()
  ((%applicators :accessor applicators
                 :initarg :applicators
                 :initform (list)
                 :type list
                 :documentation
                 "contains applicators for the schema.")

   (%annotators :accessor annotators
                :initarg :annotators
                :initform (list)
                :type list
                :documentation
                "contains annotators for the schema.")

   (%resource :initarg :resource :reader resource
              :type (not schema))))

(defmethod add-applicator ((schema schema) applicator)
  (push applicator (applicators schema)))

(defmethod find-applicator ((keyword-name string) (schema schema))
  (find keyword-name (applicators schema)
        :test #'string= :key (lambda (applicator)
                               (keyword-name (class-of applicator)))))
