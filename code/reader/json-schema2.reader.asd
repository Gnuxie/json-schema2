(asdf:defsystem #:json-schema2.reader
  :description "reader for json-schema2"
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :license  "Artistic License 2.0"
  :depends-on ("closer-mop" "cl-yaml" "defpackage-plus" "verbose" "cl-hash-util"
                            "anaphora" "json-schema2.util" "do-urlencode" "split-sequence")
  :serial t
  :components ((:file "package")
               (:file "resource")
               (:file "schema")
               (:file "applicator")
               (:file "keyword-dispatch")))
