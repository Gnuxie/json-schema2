#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:json-schema2.vocabulary)

(defclass property ()
  ((name :initarg :name
         :reader name
         :type string)

   (schema :initarg :schema
           :reader schema
           :type schema)))

(defclass properties (applicator)
  ((properties :initarg :properties
               :reader properties
               :documentation "A list of named properties to apply."))
  (:metaclass applicator-class)
  (:keyword-name "properties"))

(define-keyword "properties" (keyword resource schema)
  (declare (ignore keyword))
  (let ((properties
         (loop :for k :being :the :hash-key :of (object resource)
              :collect (make-instance 'property
                                      :name k
                                      :schema
                                      (read-schema (relative-resource resource k))))))
    (add-applicator schema (make-instance 'properties :properties properties))))

(defclass pattern-properties (applicator)
  ((properties :initarg :properties
               :reader properties
               :documentation "A list of named properties to apply"))
  (:metaclass applicator-class)
  (:keyword-name "patternProperties"))

(define-keyword "patternProperties" (keyword resource schema)
  (declare (ignore keyword))
  (let ((properties
         (loop :for k :being :the :hash-key :of (object resource)
            :collect (make-instance 'property
                                    :name k
                                    :schema
                                    (read-schema (relative-resource resource k))))))
    (add-applicator schema (make-instance 'pattern-properties :properties properties))))
