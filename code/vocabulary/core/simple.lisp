#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:json-schema2.vocabulary)

(defclass json-type (applicator)
  ((json-type :initarg :json-type
              :reader json-type))
  (:metaclass applicator-class)
  (:keyword-name "type"))

(define-keyword "type" (keyword resource schema)
  (declare (ignore keyword))
  (let ((type (object resource)))
    (add-applicator schema (make-instance 'json-type :json-type type))))

(defclass description (applicator)
  ((description :initarg :description
                :reader description
                :type string))
  (:metaclass applicator-class)
  (:keyword-name "description"))

(define-keyword "description" (keyword resource schema)
  (declare (ignore keyword))
  (add-applicator schema (make-instance 'description :description (object resource))))


(defclass title (applicator)
  ((title :initarg :title
                :reader title
                :type string))
  (:metaclass applicator-class)
  (:keyword-name "title"))

(define-keyword "title" (keyword resource schema)
  (declare (ignore keyword))
  (add-applicator schema (make-instance 'title :title (object resource))))

(defclass required (applicator)
  ((required :initarg :required
             :reader required
             :documentation "
The value of this keyword MUST be an array.  Elements of this array,
if any, MUST be strings, and MUST be unique. Yet for some goddamn reason
people keep giving this as a boolean on a propety."))
  (:metaclass applicator-class)
  (:keyword-name "required"))

(define-keyword "required" (keyword resource schema)
  (declare (ignore keyword))
  (add-applicator schema (make-instance 'required :required (object resource))))

(defclass items (applicator)
  ((item-schemas :initarg :item-schemas
                 :reader item-schemas
                 :type list))
  (:metaclass applicator-class)
  (:keyword-name "items"))

(define-keyword "items" (keyword resource schema)
  (declare (ignore keyword))
  (let ((item-schemas
         (etypecase (object resource)
           (hash-table (list (read-schema resource)))
           (list
            (loop :for item-schema :in (object resource)
               :for i :from 0
               :collect (read-schema (relative-resource resource i)))))))
    (add-applicator schema
                    (make-instance 'items :item-schemas item-schemas))))
