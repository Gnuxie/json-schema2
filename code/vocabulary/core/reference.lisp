#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:json-schema2.vocabulary)

(defclass ref (applicator)
  ((uri :initarg :uri :reader uri
        :type string :documentation "The $ref\" keyword is an applicator that is used to reference a statically identified schema. Its results are the results of the referenced schema. [CREF9]

The value of the \"$ref\" property MUST be a string which is a URI-Reference. Resolved against the current URI base, it produces the URI of the schema to apply. \""))
  (:metaclass applicator-class)
  (:keyword-name "$ref"))

(define-keyword "$ref" (keyword resource schema)
  (declare (ignore keyword))
  (add-applicator schema (make-instance 'ref :uri (object resource))))

(defclass logical-applicator (applicator)
  ((schemas :initarg :schemas :reader schemas
            :type list)))

(defclass all-of (logical-applicator) ()
  (:metaclass applicator-class)
  (:keyword-name "allOf"))

(define-keyword "allOf" (keyword resource schema)
  (declare (ignore keyword))
  (let ((schemas
         (loop :for schema :in (object resource)
              :for i :from 0 :to (length (object resource))
            :collect (read-schema (relative-resource resource i)))))
    (add-applicator schema (make-instance 'all-of :schemas schemas))))
