(asdf:defsystem #:json-schema2.vocabulary
  :description "Vocabulary"
  :license "Artistic License 2.0"
  :depends-on ("json-schema2.reader")
  :serial t
  :components ((:file "package")
               (:module "core" :components
                        ((:file "properties")
                         (:file "simple")
                         (:file "reference")))))
