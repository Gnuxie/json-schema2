#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:json-schema2.vocabulary
  (:use #:cl #:json-schema2.reader)
  (:export
   ;; properties
   #:property
   #:name
   #:schema
   #:properties
   #:pattern-properties

   ;; simple
   #:json-type
   #:description
   #:title
   #:required
   #:items
   #:item-schemas

   ;; reference
   #:ref
   #:uri
   #:logical-applicator
   #:schemas
   #:all-of
   ))
